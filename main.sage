reset()

load("helper_functions.sage")

# Choose parameters
q = 2
m = 41
u = 3
n = m
k = 27
w = 8

if (n-k)<=w:
    print '(n-k)<=w'
    assert 0

if w<=floor( (n-k)/2):
    print 'w<=floor( (n-k)/2)'
    assert 0

if floor( (n-w-k)/2 )<=0:
    print 't_pub = 0'
    assert 0

# Define all fields:
Fq = GF(q)
Fqm.<a> = GF(q^m,modulus='primitive')
Fqmu_tmp.<c> = GF(q^(m*u),modulus='primitive')
PR.<y> = Fqm[]
min_poly_c_overFq = PR(c.minpoly())
poly_list = prime_factors(min_poly_c_overFq)
minimal_polynomial_beta = poly_list[0]
Fqmu.<b> = Fqm.extension(minimal_polynomial_beta)


# 1. ----------- Key Generation -------------------

# 1.1 Generate g
g = my_rand_rank_vec(Fqm,Fq,rk=n,leng=n)
# print ext(Fqm,Fq,g,false).rank() == n

# 1.2 Generate Generator Matrix of Gabidulin code
G_gab = matrix(Fqm,k,n)
for ii in range(k):
    G_gab[ii,:] = matrix(Fqm,[g[0,j]^(q^(ii)) for j in range(n)])

# 1.3 Generate x
status = true
while status:
    x_test = matrix(Fqmu,[Fqmu.random_element() for i in range(k)])
    x_part = x_test[0,k-u:]
    x_part_mat = ext(Fqmu,Fqm,x_part,true)
    if x_part_mat.rank() == u:
        x_vec = x_test
        status = false

# 1.4 Generate z
# 1.4.1 Generate vector s1
s1 = my_rand_rank_vec(Fqm,Fq,w,w)
#print ext(Fqm,Fq,s1,false).rank() == w


# 1.4.2 Generate vector s
S_mat = matrix(Fqm,u,w)
for ii in range(u):
    S_mat[ii,:] = s1
s = ext_inv(Fqmu,Fqm,S_mat)

# 1.4.3 Generate matrix P
status = true
while status:
    P_test = matrix(Fq,[[Fq.random_element() for i in range(n)] for j in range(n)])
    if P_test.rank() == n:
        P = P_test
        status = false

# 1.4.4 Generate vector z1
s_zero = matrix(Fqmu, [list(s[0][:])+[0]*(n-w)])
z_vec = s_zero*P.inverse()

# 1.5 Generate k_pub
k_pub = x_vec*G_gab + z_vec
t_pub = floor( (n-w-k)/2 )

print 'Key Generation done'

# 2. ------------------ Encryption --------------------------
# 1. generate message vector
m_vec = matrix(Fqm, [Fqm.random_element() for ii in range(k-u)] + [0]*u)

# 2. choose random element alpha
alpha = Fqmu.random_element()

# 3. choose error vector
e_vec = my_rand_rank_vec(Fqm,Fq,t_pub,n)

# 4. Calculate ciphertext
c_vec = m_vec* G_gab + my_trace(Fqmu,Fqm,alpha * k_pub) + e_vec

print 'Encryption done'

# 3. ------------------- Decryption ---------------------------------
# 3.1 Compute cP
c_prime_long = c_vec * P
c_prime = c_prime_long[0,w:]


# 3.2 Decode
# 3.2.1 Compute G' and the corresponding parity-check matrix H'
G_prime = (G_gab*P)[:,w:]
H_prime = Gab_ParityCheckMatrix(G_prime)

# 3.2.2 Compute the syndrome
s = c_prime * H_prime.transpose()
# This is just a sagemath thing: cast s to Fqm
s = matrix(Fqm,[Fqm(s[0,ii].list()[0]) for ii in range(s.ncols())])

# 3.2.3 Apply decoding algorithm
if s == 0:
    c_dec = c_prime
else:
    t = t_pub
    n_prime = n-w

    # Set up the linear system of equations for the error span polynomial Lambda(x).
    S_t = matrix(Fqm,[[s[0,i+t-j]^(q^(j)) for j in range(t+1)] for i in range(n_prime-k-t)])

    # Solve the linear system of equations and derive the polynomial Lambda(x).
    Lam_v = S_t[:,1:].solve_right(S_t[:,0])
    Lambda = [1] + Lam_v.list()
    x = PolynomialRing(Fqm, 'x').gen() # define the variable name
    Lambda_x = sum([Lambda[i]*x^(q^i) for i in range(t+1)])

    # Find t linearly independent roots of Lambda(x) and the error value vector e_a.
    roots = Lambda_x.roots(multiplicities = False)
    roots_b = ext(Fqm,Fq,matrix(Fqm,roots),False)

    roots_echeform = roots_b.transpose().echelon_form().transpose()
    e_a = matrix(Fqm,ext_inv(Fqm,Fq,roots_echeform[:,:t]))

    #  Solve the linear system of equations to find the intermediate vector d_vec.
    d_A = matrix([[e_a[0,j]^(q^(m-i)) for j in range(t)] for i in range(n_prime-k)])
    d_b = matrix.column([s[0,i]^(q^(m-i)) for i in range(n_prime-k)])
    d_vec = d_A.solve_right(d_b).transpose()

    # Find e_B from d_vec s.t. e_B * h_0_b = d_vec_b.
    d_vec_b = ext(Fqm,Fq,matrix(Fqm,d_vec),False).transpose()

    h_0 = H_prime[0, :]
    h_0_b = ext(Fqm,Fq,matrix(Fqm,h_0),False).transpose()
    e_B = h_0_b.solve_left(d_vec_b)

    # Calculate the error vector e and the decoded codeword c_dec.
    e_hat = e_a * e_B
    c_dec = c_prime - e_hat
    # c_dec - enc_m_prime == 0 # should be true

# 3.3 Retrieve m_prime
m_prime_hat = c_dec * G_prime.transpose() * (G_prime * G_prime.transpose()).inverse()

# 3.4 Retrieve alpha
x_vec_dual = my_dual(Fqmu,x_vec[0,(k-u):])
alpha_hat = sum([m_prime_hat[0,k-u+ii]*x_vec_dual[ii] for ii in range(u)])
# alpha == alpha_hat # should be true

# 3.5 Calculate m:
m_hat = m_prime_hat - my_trace(Fqmu,Fqm,alpha*x_vec)

print('Encryted message equals the decrypted message:')
print m_hat == m_vec
