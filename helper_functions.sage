def ext(Fl,Fs,vec,is_my_extension):
    # Maps the vector vec over Fl to a matrix over Fs.
    # is_my_extension True means that Fl is a extension of an extension field, e.g., Fl = GF(2^2).extension(minimal_polynomial_beta)
    # is_my_extension False means that Fl is a 'regular' extensionfield, e.g., Fl = GF(2^6)
    u = Fl.degree()
    output = matrix(Fs,u,vec.ncols())
    if is_my_extension:
        for ii in range(vec.ncols()):
            output[:,ii] = matrix(Fs,vec[0,ii].list()).transpose()
    else:
        for ii in range(vec.ncols()):
            output[:,ii] = matrix(Fs,vec[0,ii]._vector_()).transpose()

    return output

def ext_inv(Fl,Fs,mat):
    # Maps the matrix mat over Fs to a vector over Fl
    m = Fl.degree()
    output = matrix(Fl,sum([mat[i,:]*(Fl.gen()^i) for i in range(m)]))
    return output

def my_trace(Fl,Fs,vec):
    # Trace operation from a vector vec over Fl to a projection over Fs
    val_mu = log(Fl.cardinality(),Fl.characteristic())
    val_m = log(Fs.cardinality(),Fs.characteristic())
    u = val_mu / val_m
    qm = Fs.cardinality()
    output = matrix(Fl,1,vec.ncols())
    for ii in range(vec.ncols()):
        output[0,ii] = sum([vec[0,ii]^(qm^j) for j in range(u)])
    return output


def my_rand_rank_vec(Fl,Fs,rk,leng):
    # Generate a random vector of length leng over Fl with rank rk over Fs
    u = Fl.degree()
    # 1. Compute echelonizable matrix with rank rk
    rank_false = true
    while rank_false:
        A_mat = matrix.random(Fs,u,rk)
        if A_mat.rank()==rk:
            rank_false = false
    # print A_mat

    rank_false = true
    while rank_false:
        B_mat = matrix.random(Fs,rk,leng)
        if B_mat.rank()==rk:
            rank_false = false
    # print B_mat

    out_mat = A_mat*B_mat
    out_vec = ext_inv(Fl,Fs,out_mat)

    return out_vec



def my_dual(Fq, basis):
    # Generate a dual basis
    basis = vector(basis[0,:])
    entries = [(basis[i] * basis[j]).trace() for i in range(Fq.degree()) for j in range(Fq.degree())]
    B = matrix(Fq.base_ring(), Fq.degree(), entries).inverse()
    db = [sum(map(lambda x: x[0] * x[1], zip(col, basis))) for col in B.columns()]
    return db


def my_dual2(Fq, basis):
    # Generate a dual basis
    basis = vector(basis[0,:])
    entries = [(basis[i] * basis[j]).trace() for i in range(Fq.degree()) for j in range(Fq.degree())]
    B = matrix(Fq.base_ring(), Fq.degree(), entries).inverse()
    db = []
    for i in range(Fq.degree()):
        i_vec = matrix(Fq.base_ring(),Fq.degree(),1)
        i_vec[i,0] = 1
        print i_vec
        beta = B*i_vec
        d_i = sum([beta[j,0]*basis[0,j] for j in range(Fq.degree())])
        db.append(d_i)
    print "test"
    return db



def Gab_ParityCheckMatrix(G):
    # Compute the Parity Check matrix for a generator matrix of a Gabidulin code G
    F = G.base_ring()
    q = F.base().cardinality()
    m = F.degree()
    n = G.ncols()
    k = G.nrows()
    g = G[0,:].list()

    Mhelp = matrix(F,n-1,n)
    for j in range(-n+k+1,k):
	Mhelp[j,:] = vector([g[i]^(q^(m+j)) for i in range(n)])

    hv = Mhelp[:,1:].solve_right(Mhelp[:,0])
    hv = [1] + hv.list()

    H = matrix([[hv[i]^(q^j) for i in range(n)] for j in range(n-k)])
    return H
